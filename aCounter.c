/// Lab 04a - aCounter
///
/// @file aCounter.c
/// @version 1.0
///
/// @author In Woo Park <inwoo@hawaii.edu>
/// @brief  Lab 04a - 'A' Count - EE 491F - Spr 2021
/// @date   2/09/2021
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/*
 * Method to get the start address
 */
char* get_start(char* buffy) {
	int size = strlen(buffy);
	char* start_address = malloc(size);

	for (int i = 0; i < size; ++i) {
		if (buffy[i] == '-') {
			break;
		}
		start_address[i] = buffy[i];
	}
	return start_address;
}

/*
 * Method to get the end address
 */
char* get_end(char* buffy) {
	int size = strlen(buffy);
	char* end_address = malloc(size);
	int start = 0; //bool to determine if we should add to our address
	int start_index = 0; //index of char*

	for (int i = 0; i < size; ++i) {
		if (buffy[i] == '-') { //start at '-'
			start = 1;
		continue;
		}

		if (buffy[i] == ' ') { //stop at ' '
			break;
		}

		if (start == 1) { //if we can start
			end_address[start_index] = buffy[i]; //add current value
			start_index++; //increment string index
		}
	}
	return end_address;
}

/*
 * Method to get the permissions
 */
char* get_permission(char* buffy) {
	int size = strlen(buffy);
	char* permissions = malloc(size);

	int start = 0;
	int start_index = 0;

	for (int i = 0; i < size; i++) {
		if(buffy[i] == ' ' && start == 0) {
			start = 1;
			continue;
		}

		if(buffy[i] == ' ' && start == 1) {
			break;
		}

		if(start == 1) {
			permissions[start_index] = buffy[i];
			start_index ++;
		}
	}
	return permissions;
}

/*
 * The main will read the start and end addresses and return a character counter
 * The total bytes actual word count needs debugging.
 */
int main () {

	/* Initialize Variables */
	char buffer[200];
	FILE *fp = fopen("/proc/self/maps", "r");
	char *startAddress;
	char *endAddress;
	char *permissions;
	int aCounter = 0;
	int bufferSize = 0 ;

	/* Loop to go through the addresses */
	while (fgets(buffer, sizeof(buffer), fp)) {
		startAddress = get_start(buffer);
		endAddress = get_end(buffer);
		permissions = get_permission(buffer);

		const char hex[] = "0x";
		const unsigned int RESULT_SIZE = sizeof(startAddress) + sizeof(hex) + 2 * sizeof('\0');
		char *hexStartAddress = malloc(RESULT_SIZE);
		char *hexEndAddress = malloc(RESULT_SIZE);

		/* In order to add 0x in front of the address */
		strcpy(hexStartAddress, hex);
		strcat(hexStartAddress, startAddress);

		/* In order to add 0x in front of the address */
		strcpy(hexEndAddress, hex);
		strcat(hexEndAddress, endAddress);

		bufferSize = endAddress - startAddress;

		char *value = (char*)hexStartAddress;

		/* This is where the 'A' counter actually happens */
		for(int i = 0; i < bufferSize; i++) {
			char current_byte = value[i];
			if (current_byte == 'A'){
				aCounter ++;
			}
		}

		printf("%s - %s %s Number of bytes read [%d]    Number of 'A' is [%d]\n",
                hexStartAddress, hexEndAddress, permissions, bufferSize, aCounter);
		memset(buffer, 0, sizeof(buffer));
		fflush(stdin);
	}
	fclose(fp);
	return 0;
}


